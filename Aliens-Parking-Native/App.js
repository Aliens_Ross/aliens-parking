import React from 'react';
import { StyleSheet, Text, View, AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';
import Login from './screens/Login';
import ParkingMain from "./screens/ParkingMain";
import * as firebase from 'firebase';
import config from './firebase/private-key.json';

firebase.initializeApp(config);

var makeConnection = firebase.database().ref();

export default class App extends React.Component {

constructor(props){
  super(props);

  this.state = {
    contentView: false
  }

  this.openContent = this.openContent.bind(this);
}

  openContent() {
    this.setState({contentView: true});
  }

  render() {
    return (
        this.state.contentView ? <ParkingMain style={styles.container} /> : <Login style={styles.container} openContent={this.openContent}/>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 24, 
  }
});
