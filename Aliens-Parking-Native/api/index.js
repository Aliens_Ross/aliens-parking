var token = "xoxp-7600129015-149277569557-185818570598-8f9c755aba598b2654078ec8510ede4d";

function getSlackUsers(callback) {
	var users = null;
	var response = null;

	var url = "https://slack.com/api/users.list?token=" + token;

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200) {
			response = JSON.parse(xmlHttp.responseText);
			if (response.ok === false) {
				console.error("Unable to retrieve user list.");
			}
			else {
				users = response.members;
			}
			callback(users);
		}
		else if (this.readyState === 4 && this.status !== 200){
			console.error("Unable to retrieve user list.");
		}
    };
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

function getSlackUser(identifier, callback) {
	var user = null;

	getSlackUsers((users) => {
		if (users !== null) {
			users.forEach((u) => {
				if (u.name === identifier || u.profile.email === identifier) {
					user = u;
				}
			});
		}
		callback(user);
	});
}

export {getSlackUsers, getSlackUser};
