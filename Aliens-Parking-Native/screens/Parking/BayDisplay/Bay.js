import React from 'react';
import { StyleSheet, Text, View, Image, ListView, TouchableHighlight} from 'react-native';
import picture from '../../../random.png';
import * as firebase from 'firebase';

const styles = StyleSheet.create({
  container: {
    padding: 12,
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: "#000",
    marginTop: 5,
  },
  parkingBay: {
    padding: 12,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: "#000",
    marginTop: 5,
    marginLeft: 2,
  },
  AreaText: {
    marginLeft: 12,
    fontSize: 16,
    marginBottom: 5,
  },
  photo: {
    height: 40,
    width: 40,
    marginTop: 5,
  },
  ParkingsText: {
    marginLeft: 12
  },
  contentContainer: { 
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  parkingBayText: {
    padding: "6.5%",
  },
  booked: {
    padding: 12,
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: "#000",
    marginTop: 5,
    backgroundColor: "#f00",
  },
  reported: {
    padding: 12,
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: "#000",
    marginTop: 5,
    backgroundColor: "#ff0",
  },
});

class Bay extends React.Component{
  
  constructor(props){
      super(props);
  }

  goTo(screen){
    this.props.navigation.navigate('AreaDisplay');
  }

  selectParkingBay(selectedBay, bayStatus){
      if(bayStatus === 1 || bayStatus === 2){
        alert("Well...");
      }else{
        var bayData = {
              time: Math.floor(Date.now() / 1000),
              status: 1
          };
          var key = firebase.database().ref().push().key;
          var updates={};
          updates['/parking/' + this.props.selectedArea + '/' + selectedBay] = bayData;

          return firebase.database().ref().update(updates);
      }
        
    }

    bayStyle(status){
        var style = "";
        switch(status){
            case 0:
                style = styles.parkingBay;
                break;
            case 1:
                style = styles.booked;
                break;
            case 2:
                style = styles.reported;
                break;
        }
        return style;
    }

  render(){
      return (
        <TouchableHighlight 
          onPress={() => this.selectParkingBay(this.props.rowData.key, this.props.rowData.child("status").val())}>  
            <View style={this.bayStyle(this.props.rowData.child("status").val())}>
                <Text style={styles.parkingBayText}>
                    {this.props.rowData.key}
                </Text>
            </View>
        </TouchableHighlight>
        )
    }
}

export default Bay;



