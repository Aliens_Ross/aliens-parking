import React from 'react';
import { StyleSheet, Text, View, Image, ListView, TouchableHighlight} from 'react-native';
import { Button } from 'react-native-elements';
import picture from '../../../random.png';
import * as firebase from 'firebase';
import Bay from './Bay.js';

const styles = StyleSheet.create({
  container: {
    padding: 12,
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: "#000",
    marginTop: 5,
  },
  parkingBay: {
    padding: 12,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: "#000",
    marginTop: 5,
    marginLeft: 2,
  },
  AreaText: {
    marginLeft: 12,
    fontSize: 16,
    marginBottom: 5,
  },
  photo: {
    height: 40,
    width: 40,
    marginTop: 5,
  },
  ParkingsText: {
    marginLeft: 12
  },
  contentContainer: { 
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  parkingBayText: {
    padding: "6.5%",
  }
});

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});


export default class BayDisplay extends React.Component {

  static navigationOptions = {
    title: "BayDisplay"
  }

  constructor(props){
    super(props);
    this.state = { 
     dataSource: ds,  
     allAvailBays: 0,
    };

  }

  goTo(screen){
    this.props.navigation.navigate('AreaDisplay');
  }

  componentWillMount(){
    this.refreshAreaDisplay();
  }

 refreshAreaDisplay(){
    var areaRef = firebase.database().ref('parking/' + this.props.navigation.state.params.parkingName);

    areaRef.on('value', (snapshot) => {
      var availBays = 0;
      var areaBays = [];
      snapshot.forEach((childSnap) => {
        areaBays.push(childSnap);
        if(childSnap.child("status").val() === 0){
          availBays++;
        }
      });

      this.setState({
        dataSource: ds.cloneWithRows(areaBays), 
        allAvailBays: availBays
      });
    });
  }
    
  
  render() {
    return (
      <View>
          <View style={styles.container}>
            <Image source={picture} style={styles.photo}/>
            <View>
              <Text style={styles.AreaText}> 
                {this.props.navigation.state.params.parkingName}
              </Text>
              <Text style={styles.ParkingsText}> 
                {this.state.allAvailBays} Parkings Available
              </Text>
            </View>
          </View>
          <ListView
            removeClippedSubviews={false}
            contentContainerStyle={styles.contentContainer}
            dataSource={this.state.dataSource} 
            renderRow={(bay) => 
              <Bay rowData={bay} selectedArea={this.props.navigation.state.params.parkingName}/> 
            }/>
      </View>
    );
  }
}
