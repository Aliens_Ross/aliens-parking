import React from 'react';
import { StyleSheet, Text, View, Image, ListView, TouchableHighlight} from 'react-native';
import picture from '../../../random.png';
import * as firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay'; 

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 17,
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderColor: "#000",
    marginTop: 5,
  },
  AreaText: {
    marginLeft: 12,
    fontSize: 16,
    marginBottom: 5,
  },
  photo: {
    height: 40,
    width: 40,
    marginTop: 5,
  },
  ParkingsText: {
    marginLeft: 12
  }, 
});

export default class AreaDisplay extends React.Component {

  constructor(props){
    super(props);
     this.state = { 
       loading: true,
       dataSource: ds,
       availBays: [],
      };
  }

  componentWillMount() {
    this.refreshAreaDisplay();
  }

  static navigationOptions = {
    title: "AreaDisplay",
  }

  async refreshAreaDisplay(){
    var areasRef = firebase.database().ref('parking/');
    var totals = [];
    var areas = [];
    
    areasRef.on('value', (snapshot) => {
      var i = 0;
      areas = [];
      totals = [];
    
      snapshot.forEach((childSnap) => {
        var temp = 0;
        areas.push(childSnap.key);
        childSnap.forEach((thirdChildSnap) =>{
          if(thirdChildSnap.child("status").val() === 0){
            temp++;
          }
        });
        totals[i] = temp;
        i++;    
      });

      this.setState({
        availBays: totals, 
        dataSource: ds.cloneWithRows(areas),
        loading: false
      });
    });
  }

  goTo(screen){
    this.props.navigation.navigate('BayDisplay', {parkingName: screen});
  }

  render() {
    return (
    <View>
      <Spinner 
          visible={this.state.loading} 
          textContent={"Loading..."} 
          textStyle={{color: '#FFF'}}/> 
      <ListView
        dataSource={this.state.dataSource} 
        renderRow={(rowData, sec, i) =>  
        <TouchableHighlight onPress={() => this.goTo(rowData)}>  
          <View style={styles.container}>
            <Image source={picture} style={styles.photo} />
            <View>
              <Text style={styles.AreaText}> 
                {rowData} 
              </Text>
              <Text style={styles.ParkingsText}> 
                {this.state.availBays[i]}
              </Text>
            </View>
          </View>
        </TouchableHighlight>
        }/>
    </View>
        );
      }
    }