import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import BayDisplay from '../Parking/BayDisplay';
import AreaDisplay from '../Parking/AreaDisplay';

const Navigator = StackNavigator({
  AreaDisplay: {
    screen: AreaDisplay,
  },
  BayDisplay: {
    screen: BayDisplay,
  },
})

export default class Parking extends React.Component {

  static navigationOptions = {
    title: "Parking"
  }
  
  render() {
    return (
     <Navigator/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center'
  },
});
