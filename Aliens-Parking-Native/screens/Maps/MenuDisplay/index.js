import React from 'react';
import { StyleSheet, Text, View, Button, Image, Easing, Dimensions } from 'react-native';
import DropdownMenu from 'react-native-dropdown-menu';
import ImageZoom from 'react-native-image-pan-zoom';

import Map1 from '../../../Assets/Maps/Map1.jpeg';
import Map2 from '../../../Assets/Maps/Map2.jpeg';
import Map3 from '../../../Assets/Maps/Map3.jpeg';
import Map4 from '../../../Assets/Maps/Map4.jpeg';
  
export default class MenuDisplay extends React.Component {

  data = [["Cobblestone", "Darter's", "Spar Top", "Spar Bottom"]];
  images = [Map1, Map2, Map3, Map4];

  static navigationOptions = {
    title: "Maps"
  }

  constructor(props){
    super(props);

    this.state = {
      map: 0
    }

    this.changeMap = this.changeMap.bind(this);
  }

  changeMap(index) {
    this.setState({
      map: index
    })
  }

  render() {
    return (
      <DropdownMenu 
        style={styles.dropdown}
        data={this.data}                                
        maxHeight={400}
        handler={(selection, row) => this.changeMap(row)} >
          <View style={styles.content} >
              <ImageZoom
                cropWidth={Dimensions.get('window').width}
                cropHeight={Dimensions.get('window').height}
                imageWidth={Dimensions.get('window').width}
                imageHeight={Dimensions.get('window').height}>
                  <Image 
                    style={styles.imageZoomView}
                    source={this.images[this.state.map]}/>
              </ImageZoom>
          </View>
      </DropdownMenu> 
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  imageZoomView: {
    width:Dimensions.get('window').width, 
    height:Dimensions.get('window').height, 
    alignContent: 'center'
  }
});
