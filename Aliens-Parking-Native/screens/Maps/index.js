import React from 'react';
import { StackNavigator } from 'react-navigation';
import MenuDisplay from './MenuDisplay';

const StackNav = StackNavigator({
  Maps: {
    screen:  MenuDisplay,
  }
})

export default class Maps extends React.Component {
  render() {
    return (
      <StackNav />
    );
  }
}