import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

import LoginDisplay from './LoginDisplay';
import Registration from './Registration';

  const StackNav = StackNavigator({
    Login: {
        screen:  LoginDisplay,
    },
    Register: {
        screen: Registration
    },
  })

export default class Login extends React.Component {
  constructor(props){
    super(props)
    // props.openContent();
  }
  render() {
    return (
      <StackNav screenProps={{ openContent: this.props.openContent }} />
    );
  }
}
