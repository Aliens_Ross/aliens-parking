import React from 'react';
import { StyleSheet, Text, View, Image, Modal } from 'react-native';
// import Modal from 'react-native-modal';
import {
	FormLabel,
	FormInput,
	Button,
	FormValidationMessage
} from 'react-native-elements';
import {getSlackUser} from '../../api';
import Spinner from 'react-native-loading-spinner-overlay';
import * as firebase from 'firebase';

export default class SignUp extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			pass: '',
			confirmPass: '',
			emailError: null,
			passError: null,
			pass2Error: null,
			registering: false,
			successful: null,
		};

		this.signUp = this.signUp.bind(this);
		this.focus = this.focus.bind(this);
		this.storeNewUser = this.storeNewUser.bind(this);
		this.updateUsername = this.updateUsername.bind(this);
	}

	static navigationOptions = {
		title: "Sign Up"
	}

	focus() {

	}

	updateUsername(user, username) {
		firebase.database().ref('users/' + user.uid).set({
			slackName: username
		}).then(() => {
			this.setState({successful: true, registering: false});
		})
		.catch((error) => {
			console.error(error.code + ":", error.message);
			user.delete().then(function() {
				this.setState({successful: false, registering: false});
			}, function(del_error) {
				console.error(del_error.code + ":", del_error.message);
				this.setState({successful: false, registering: false});
			});
		});
	}

	async storeNewUser(user) {
		var firebaseUser = null;
		var errMessage = null;

		if (user === null || user === undefined) {
			errMessage = (
				<FormValidationMessage>
					Please check that you have entered a valid @cowboyaliens.com email address.
				</FormValidationMessage>
			);
			this.setState({emailError: errMessage, registering: false});
		}
		else {
			firebaseUser = await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.pass)
			.catch(function(error) {
				if (error.code === 'auth/email-already-in-use' ||
					error.code === 'auth/invalid-email') {
						errMessage = (
							<FormValidationMessage>
								{error.message}
							</FormValidationMessage>
						);
						this.setState({emailError: errMessage, registering: false});
				}
				else if (error.code === 'auth/weak-password') {
					errMessage = (
						<FormValidationMessage>
							{error.message}
						</FormValidationMessage>
					);
					this.setState({passError: errMessage, registering: false});
				}
				else {
					console.error(error.code + ':',  error.message);
					this.setState({registering: false});
				}
			}.bind(this));
			if (firebaseUser) {
				firebaseUser.updateProfile({
					displayName: user.profile.real_name_normalized,
				}).then(() => {
					this.updateUsername(firebaseUser, user.name);
				},
				(error) => {
					this.setState({successful: false, registering: false});
				});
			}
			else {
				if (this.state.emailError || this.state.passError)
					this.setState({successful: null, registering: false});
				else
					this.setState({successful: false, registering: false});
			}
		}
	}

	signUp() {
		if (this.validateForm() === true) {
			this.setState({registering: true});
			getSlackUser(this.state.email, this.storeNewUser);
		}
	}

	validateForm() {
		let errMessage = null;

		this.setState({emailError: null, passError: null, pass2Error: null});
		if (/.*@cowboyaliens\.com$/.test(this.state.email) === false) {
			errMessage = (
				<FormValidationMessage>
					Please check that you have entered a valid @cowboyaliens.com email address.
				</FormValidationMessage>
			);
			this.setState({emailError: errMessage});
			return false;
		}
		else if (this.state.pass.length < 6) {
			errMessage = (
				<FormValidationMessage>
					Password needs to be at least 6 characters long.
				</FormValidationMessage>
			);
			this.setState({passError: errMessage});
			return false;
		}
		else if (this.state.confirmPass !== this.state.pass) {
			errMessage = (
				<FormValidationMessage>
					Passwords do not match.
				</FormValidationMessage>
			);
			this.setState({pass2Error: errMessage});
			return false;
		}

		return true;
	}

	render() {
		let successful = null;
		let modal_message = null;
		let style = null;

		if (this.state.successful === true)
			style = styles.success;
		else if (this.state.successful === false)
			style = style.error;

		if (this.state.successful === true) {
			modal_message = (
				<View>
					<Text style={styles.heading}>
						Welcome to
					</Text>
					<Text style={styles.heading}>
						BayWatch
					</Text>
					<Text style={styles.heading}>
						{firebase.auth().currentUser.displayName}!
					</Text>
				</View>
			);
		}
		else if (this.state.successful === false) {
			modal_message = (
				<Text>Soooo... Something went wrong and we were unable to create your account :(... Sorry about that.</Text>
			);
		}

	    return (
			<View style={styles.container}>
				<FormInput
					containerStyle={styles.textInput}
					placeholder='Email address...'
					autoCapitalize='none'
					autoFocus={true}
					keyboardType='email-address'
					returnKeyType={'next'}
					onChangeText={(email) => this.setState({email})}
				/>
				{this.state.emailError}

				<FormInput
					containerStyle={styles.textInput}
					placeholder='Password...'
					autoCapitalize='none'
					returnKeyType={'next'}
					secureTextEntry={true}
					onChangeText={(pass) => this.setState({pass})}
				/>
				{this.state.passError}

				<FormInput
					containerStyle={styles.textInput}
					placeholder='Confirm password...'
					autoCapitalize='none'
					returnKeyType={'done'}
					secureTextEntry={true}
					onChangeText={(confirmPass) => this.setState({confirmPass})}
				/>
				{this.state.pass2Error}

				<Button
					onPress={() => this.signUp()}
					title='Sign Up'
					accessibilityLabel='Sign Up'
				/>
				<Spinner
					visible={this.state.registering}
					textContent={"Signing Up..."}
					textStyle={{color: '#FFF'}}
				/>
				<Modal
					visible={this.state.successful !== null}
					onRequestClose={() => this.setState({successful: null})}
					transparent={false}
					animationType={'fade'}
				>
					<View
						style={style}
					>
						{modal_message}
						<Button
							onPress={() => this.setState({successful: null})}
							title='Okay'
							accessibilityLabel='Okay'
							style={styles.modal_button}
						/>
					</View>
				</Modal>
			</View>
	    );
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
	},

	textInput: {
		width: '80%',
		margin: 10,
	},

	heading: {
		fontSize: 28,
		textAlign: 'center',
	},

	error: {
		flex: 1,
		alignContent: 'center',
		backgroundColor: 'rgba(200,0,0,0.1)',
		justifyContent: 'center',
		padding: '15%'
	},

	success: {
		flex: 1,
		alignContent: 'center',
		backgroundColor: 'rgba(0,200,0,0.1)',
		justifyContent: 'center',
		padding: '15%'
	},

	modal_button: {
	margin: 15,
	}
});
