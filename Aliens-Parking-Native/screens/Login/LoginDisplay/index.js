import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import * as Firebase from 'firebase';

export default class LoginDisplay extends React.Component {
  
  static navigationOptions = {
    title: "BAYWATCH",
    header: null
  }

  constructor(props){
      super(props)
      this.state = {
          email: "ross@cowboyaliens.com",
          password: "aweawe"
      }

  }

  loginUser(){
    Firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {
      this.props.screenProps.openContent();
    })
  }

  updateField(value) {
    this.setState(value);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}> Login </Text>
        <TextInput
            style={styles.email}
            placeholder={"email"}
            value={this.state.email}
            onChange={(e) => this.updateField({email: e.nativeEvent.text})}
            underlineColorAndroid='#eee'/>
        <TextInput
            style={styles.password}
            placeholder={"password"}
            value={this.state.password}
            onChange={(e) => this.updateField({password: e.nativeEvent.text})}
            underlineColorAndroid='#eee'/>
          <Button
            onPress={this.loginUser.bind(this)}
            title="Login"
          />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop: 24
  },
  title: {
    fontSize: 48
  },
  email: {
    width: "80%",

  },
  password: {
    width: "80%",

  }
});
