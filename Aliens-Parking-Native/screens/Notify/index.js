import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

import NotifyDisplay from './NotifyDisplay';

  const StackNav = StackNavigator({
    Notify: {
      screen:  NotifyDisplay
    }
  })

export default class Notify extends React.Component {
  render() {
    return (
      <StackNav />
    );
  }
}