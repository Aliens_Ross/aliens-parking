import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default class NotifyDisplay extends React.Component {

  static navigationOptions = {
    title: "Notify",
  }

  render() {
    return (
      <View style={styles.container}>
          <Text> Notify to be Edited Soon </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});
