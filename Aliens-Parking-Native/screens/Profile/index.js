import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

import ProfileDisplay from './ProfileDisplay';

  const StackNav = StackNavigator({
    Profile: {
      screen:  ProfileDisplay
    }
  })

export default class Profile extends React.Component {
  render() {
    return (
      <StackNav />
    );
  }
}
