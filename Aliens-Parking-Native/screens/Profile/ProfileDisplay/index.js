import React from 'react';
import { StyleSheet, Text, View, Button, Image, TouchableOpacity, TextInput, KeyboardAvoidingView, Alert, ScrollView } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import * as Firebase from 'firebase'

export default class ProfileDisplay extends React.Component {

  static navigationOptions = {
    title: "Profile",
  }

  constructor(props){
    super(props);

    this.state = {
      saving: false,
      editable: false,
      numberPlate: "",
      carType: "",
      carColor: "",
      userName: Firebase.auth().currentUser.displayName,
      numberPlate2: "",
      numberPlate3: "",
      profilePictureUrl: Firebase.auth().currentUser.photoURL,
      userUID: Firebase.auth().currentUser.uid
    }

    // alert(Firebase.auth().currentUser.uid)
    alert(this.state.userUID)
    
  }

  componentWillMount(){
    this.getProfile(this.state.userUID);
  }

  saveDetails() {
    this.setState({saving: true});
    var update = {
      numberPlate: this.state.numberPlate,
      carType: this.state.carType,
      carColor: this.state.carColor,
      numberPlate2: this.state.numberPlate2,
      numberPlate3: this.state.numberPlate3,
    }
    Firebase.database().ref("/users").child(this.state.userUID).update(update,
      (response) => {
        Alert.alert(
          "Success!",
          "Profile Information was successfully saved.",
          [
            {text: "Ok", onPress: () => this.setState({saving: false})}
          ],
          { cancelable: false }
        )
    });
  }

  assignProfile(data){
    if (data){
      console.log(data);
    }
    // this.setState({
    //     numberPlate: data.numberPlate ,
    //     numberPlate2: data.numberPlate2 ? data.numberPlate2 : "",
    //     numberPlate3: data.numberPlate3 ? data.numberPlate3 : "",
    //     carType: data.carType ? data.carType : "",
    //     carColor: data.carColor ? data.carColor : "",
    // });
  }

  async getProfile(uid) {
    return await Firebase.database().ref("/users").child(uid).on('value',(snapshot) => {
      this.assignProfile(snapshot.val());
    })
  }

  toggleEdit() {
    this.setState({editable: !this.state.editable});
  }

  updateField(value) {
    this.setState(value);
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="position">
        <Spinner visible={this.state.saving}/>
        <View style={styles.contentHeader}>
          <Image style={styles.profilePicture}
            source={require('../../../Assets/ProfilePicTemp.jpeg')}
          />
          <View style={styles.headerTextContainer}>
            <Text style={styles.nameText}>{this.state.userName}</Text>
            <Text>Logout</Text>
          </View>
        </View>

        <View style={styles.rowLine}></View>

        <ScrollView contentContainerStyle={{paddingBottom: 100}} style={{flex: 0}}>
          <View style={styles.editView}>
            <TouchableOpacity style={styles.editHighlight} onPress={this.toggleEdit.bind(this)}>
              <Text style={styles.editHighlightText}>edit</Text>
            </TouchableOpacity>
          </View>

          <View>
            <TextInput 
              style={styles.defaultTextInput}
              editable={this.state.editable}
              placeholder={"Number Plate"}
              onChange={(e) => this.updateField({numberPlate: e.nativeEvent.text})}
              value={this.state.numberPlate}
              underlineColorAndroid='transparent'/>
            <TextInput 
              style={styles.defaultTextInput}
              editable={this.state.editable}            
              placeholder={"Car Type"}
              onChange={(e) => this.updateField({carType: e.nativeEvent.text})}
              value={this.state.carType}
              underlineColorAndroid='transparent'/>          
            <TextInput 
              style={styles.defaultTextInput}
              editable={this.state.editable}   
              placeholder={"Car Color"}
              onChange={(e) => this.updateField({carColor: e.nativeEvent.text})}
              value={this.state.carColor}
              underlineColorAndroid='transparent'/>

            <Text style={styles.defaultText}>
              Add additional vehicles
            </Text>

            <View style={styles.extraNumberplatesContainer}>
              {/*To be replaced with image of Plus*/}
              <Text style={styles.plus}>
                +
              </Text>
              <TextInput 
                style={styles.extraNumberplatesInput}
                editable={this.state.editable}   
                placeholder={"Number Plate"}
                onChange={(e) => this.updateField({numberPlate2: e.nativeEvent.text})}
                value={this.state.numberPlate2}
                underlineColorAndroid='transparent'/>
            </View>
            <View style={styles.extraNumberplatesContainer}>
              {/*To be replaced with image of Plus*/}
              <Text style={styles.plus}>
                +
              </Text>
              <TextInput 
                style={styles.extraNumberplatesInput}
                editable={this.state.editable}   
                placeholder={"Number Plate"}
                onChange={(e) => this.updateField({numberPlate3: e.nativeEvent.text})}
                value={this.state.numberPlate3}
                underlineColorAndroid='transparent'/>
            </View>                    
          </View>
          <Button
            style={styles.saveButton}
            onPress={this.saveDetails.bind(this)}
            title={"Save"} />

          </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 30,
    paddingTop: 30,
    paddingRight: 30
  },
  contentHeader: {
    flexDirection: "row"
  },
  profilePicture: {
    width: 75,
    height: 75,
    borderRadius: 40
  },
  nameText: {
    fontSize: 28
  },
  headerTextContainer: {
    marginLeft: 25,
    marginTop: 10
  },
  rowLine: {
    marginTop: 20,
    height: 1,
    width: "100%",
    backgroundColor: "#ccc"
  },
  editView: {
    alignItems: "flex-end"
  },
  editHighlight: {
    width:30,
    marginTop: 5,
  },
  editHighlightText: {
    color: "#2196F3",
    fontSize: 16
  },
  defaultTextInput: {
    height: 24,
    fontSize: 24,
    marginBottom: 15,
    flexWrap: "wrap",
    flex: 1
  },
  defaultText: {
    fontSize: 24,
  },
  plus: {
    fontSize: 34
  },
  extraNumberplatesContainer: {
    flexDirection: "row"
  },
  extraNumberplatesInput: {
    height: 24,
    fontSize: 24,
    flex: 1,
    marginTop: 16,
    marginLeft: 10
  },
  saveButton: {

  },
});