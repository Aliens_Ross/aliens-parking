import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TabNavigator } from 'react-navigation';

import Parking from './../Parking';
import Maps from './../Maps';
import Notify from './../Notify';
import Profile from './../Profile';

const TabNav = TabNavigator({
  Parking: {
    screen: Parking,
  },
  Maps: {
    screen: Maps,
  },
  Notify: {
    screen: Notify,
  },
  Profile: {
    screen: Profile,
  },
}, {
  tabBarPosition: 'bottom',
  tabBarOptions: {
    activeTintColor: '#cccccc',
    labelStyle: {
      fontSize: 12,
      marginLeft: 0,
      marginRight: 0
    },
  },
});

export default class ParkingMain extends React.Component {
  
  static navigationOptions = {
    title: "Parking"
  }

  render() {
    return (
      <View style={styles.container}>
        <TabNav />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 24,
    justifyContent: 'center',
  },
  titleBar: {
    width: "100%",
    height: 56,
    backgroundColor:"#fff"
  }
});
